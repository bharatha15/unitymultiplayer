﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Launcher::Awake()
extern void Launcher_Awake_m51131F9D53E49BF22DBF64A5B3CC40A475058D1A (void);
// 0x00000002 System.Void Launcher::Start()
extern void Launcher_Start_mBD1E6306A64712A170916699B4BE6AA6BD32E37E (void);
// 0x00000003 System.Void Launcher::Update()
extern void Launcher_Update_mD70990363AAEAB75790E996E40E5DADEC5D73C11 (void);
// 0x00000004 System.Void Launcher::OnConnectedToMaster()
extern void Launcher_OnConnectedToMaster_mBE4CB3AA2DF5AF2BE55F7A31C7520372408F75A5 (void);
// 0x00000005 System.Void Launcher::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void Launcher_OnDisconnected_m00637F435FF952B3D2CDB83A808F50150695D4A4 (void);
// 0x00000006 System.Void Launcher::Connect()
extern void Launcher_Connect_m4AF199D6BD9C325D0750800B2980D666029AE718 (void);
// 0x00000007 System.Void Launcher::.ctor()
extern void Launcher__ctor_mB177F9E0B472FC0208A5EF8E702BD3712C0AED96 (void);
// 0x00000008 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::Start()
extern void ConnectAndJoinRandomLb_Start_m8784F9A942632995E527C9AAC79D40049C1B4421 (void);
// 0x00000009 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::Update()
extern void ConnectAndJoinRandomLb_Update_m27F506BAFEF2C3264B9030218D0ADB86478750E2 (void);
// 0x0000000A System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnConnected()
extern void ConnectAndJoinRandomLb_OnConnected_m5879B131543322FA6CC2D9FB2B4C196698680114 (void);
// 0x0000000B System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnConnectedToMaster()
extern void ConnectAndJoinRandomLb_OnConnectedToMaster_mCCF5E7C0A7A1A8486F3571F05B2E0662C7C939F2 (void);
// 0x0000000C System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void ConnectAndJoinRandomLb_OnDisconnected_m2576DA0537A899046151D7FA51E25AC367F800A7 (void);
// 0x0000000D System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCustomAuthenticationResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ConnectAndJoinRandomLb_OnCustomAuthenticationResponse_m8F6E456A2C572923AD8E720088C22588A1DB1459 (void);
// 0x0000000E System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCustomAuthenticationFailed(System.String)
extern void ConnectAndJoinRandomLb_OnCustomAuthenticationFailed_m447EA30CA679C12865C767F01D060A3A2EF74C5B (void);
// 0x0000000F System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRegionListReceived(Photon.Realtime.RegionHandler)
extern void ConnectAndJoinRandomLb_OnRegionListReceived_m6F54D1CDC5C51EF9ED8C44E439E83210851CDA8E (void);
// 0x00000010 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
extern void ConnectAndJoinRandomLb_OnRoomListUpdate_m91AEF2D4CB5CC06810FED71B26B736DDC2938DA0 (void);
// 0x00000011 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLobbyStatisticsUpdate(System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>)
extern void ConnectAndJoinRandomLb_OnLobbyStatisticsUpdate_m02972ED26C4B5FCECE1E7A0B482A32E53363F2F7 (void);
// 0x00000012 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinedLobby()
extern void ConnectAndJoinRandomLb_OnJoinedLobby_m171BA1094BCA0FC7A1D13EA07C858CD2D2F423EF (void);
// 0x00000013 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLeftLobby()
extern void ConnectAndJoinRandomLb_OnLeftLobby_m1BCA515302514D0EF88687D2216D67A9D724B0F4 (void);
// 0x00000014 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnFriendListUpdate(System.Collections.Generic.List`1<Photon.Realtime.FriendInfo>)
extern void ConnectAndJoinRandomLb_OnFriendListUpdate_m6E1EBEFEC84BA9FFA5296B547431BDB692E053DB (void);
// 0x00000015 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCreatedRoom()
extern void ConnectAndJoinRandomLb_OnCreatedRoom_mB1906ADF16C593829B0DD191F319716A97B10583 (void);
// 0x00000016 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCreateRoomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnCreateRoomFailed_mD11765F8AA8A45B8AC5555207281A43ABA829D24 (void);
// 0x00000017 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinedRoom()
extern void ConnectAndJoinRandomLb_OnJoinedRoom_mCE9BDE69A13EA84E9ACA73E064A695B9D3754E8B (void);
// 0x00000018 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinRoomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnJoinRoomFailed_m5622896774D36D4960735E32F353869D9D6DF94C (void);
// 0x00000019 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinRandomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnJoinRandomFailed_m4BCA35CBE444C5AD28D7D713E92E893198F91B69 (void);
// 0x0000001A System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLeftRoom()
extern void ConnectAndJoinRandomLb_OnLeftRoom_m488901B35215F9B70BACC7A08ABB123F3B605137 (void);
// 0x0000001B System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRegionPingCompleted(Photon.Realtime.RegionHandler)
extern void ConnectAndJoinRandomLb_OnRegionPingCompleted_mD76EE8F39A7AE8EF7E071B9E1C5D1A52452FEF17 (void);
// 0x0000001C System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::.ctor()
extern void ConnectAndJoinRandomLb__ctor_mD06217DD59635BD5DC39E8E14274D6604FDF85C1 (void);
// 0x0000001D Photon.Chat.ChatAppSettings Photon.Chat.Demo.AppSettingsExtensions::GetChatSettings(Photon.Realtime.AppSettings)
extern void AppSettingsExtensions_GetChatSettings_m32D1D0AF37E161CF26E9180F659F6657F6F24D00 (void);
// 0x0000001E System.Void Photon.Chat.Demo.ChannelSelector::SetChannel(System.String)
extern void ChannelSelector_SetChannel_m29F99876C0AF817AEFBDB5539DF70F23558D2A7E (void);
// 0x0000001F System.Void Photon.Chat.Demo.ChannelSelector::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void ChannelSelector_OnPointerClick_mA5A9AB30717F59D434855F47CAF95543C7A64B9C (void);
// 0x00000020 System.Void Photon.Chat.Demo.ChannelSelector::.ctor()
extern void ChannelSelector__ctor_mB3DF2FA2ED74E01976D4D3ACFB50CB096FEF18F0 (void);
// 0x00000021 System.Void Photon.Chat.Demo.ChatAppIdCheckerUI::Update()
extern void ChatAppIdCheckerUI_Update_m1D3579AE4EE35E468A353AA67EBD5D9F7C87E2E9 (void);
// 0x00000022 System.Void Photon.Chat.Demo.ChatAppIdCheckerUI::.ctor()
extern void ChatAppIdCheckerUI__ctor_mA1D182C1AED55533944CD356D13AA447574B7870 (void);
// 0x00000023 System.String Photon.Chat.Demo.ChatGui::get_UserName()
extern void ChatGui_get_UserName_mE13C9B7C725EC8F90975DE2D4ED7F3CC28596ACF (void);
// 0x00000024 System.Void Photon.Chat.Demo.ChatGui::set_UserName(System.String)
extern void ChatGui_set_UserName_m8955E5E94B1B8469E4FCD87D10041F53A80E9E0B (void);
// 0x00000025 System.Void Photon.Chat.Demo.ChatGui::Start()
extern void ChatGui_Start_m9C36C012AF6740959AD6855FCE3C7156D42B636A (void);
// 0x00000026 System.Void Photon.Chat.Demo.ChatGui::Connect()
extern void ChatGui_Connect_mF3437B3F3E848B574D498715B4FB45CBB8A769CD (void);
// 0x00000027 System.Void Photon.Chat.Demo.ChatGui::OnDestroy()
extern void ChatGui_OnDestroy_mA771028772B2BE041F6A8611CB67E6E369654D8E (void);
// 0x00000028 System.Void Photon.Chat.Demo.ChatGui::OnApplicationQuit()
extern void ChatGui_OnApplicationQuit_mCF2A69BA18FF51991BB0F7F5A6898B9C8D19B099 (void);
// 0x00000029 System.Void Photon.Chat.Demo.ChatGui::Update()
extern void ChatGui_Update_mF777D28857ABC30C851D6DD32B3A36BBE7880958 (void);
// 0x0000002A System.Void Photon.Chat.Demo.ChatGui::OnEnterSend()
extern void ChatGui_OnEnterSend_mD1E43BC5C5ED3F65C8287C7CCB2D09BE0E508375 (void);
// 0x0000002B System.Void Photon.Chat.Demo.ChatGui::OnClickSend()
extern void ChatGui_OnClickSend_mA34A7BDF93AF080B0926A4D503AC77035C6345D7 (void);
// 0x0000002C System.Void Photon.Chat.Demo.ChatGui::SendChatMessage(System.String)
extern void ChatGui_SendChatMessage_mC76BD609B9B8E024027BD69E1915E4577B65E76F (void);
// 0x0000002D System.Void Photon.Chat.Demo.ChatGui::PostHelpToCurrentChannel()
extern void ChatGui_PostHelpToCurrentChannel_m14AD0594316C6C38A2B85F390B52595A1CF8FD17 (void);
// 0x0000002E System.Void Photon.Chat.Demo.ChatGui::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void ChatGui_DebugReturn_m7E7552B5CD94F91CE5C41CD7813EDE70B6176567 (void);
// 0x0000002F System.Void Photon.Chat.Demo.ChatGui::OnConnected()
extern void ChatGui_OnConnected_m63335F6ADB99CA4C1DBD70CE3D3BE669DE0E53B9 (void);
// 0x00000030 System.Void Photon.Chat.Demo.ChatGui::OnDisconnected()
extern void ChatGui_OnDisconnected_m4AAE28BB7B47BAD132651AFC8E27F573A36E3AFF (void);
// 0x00000031 System.Void Photon.Chat.Demo.ChatGui::OnChatStateChange(Photon.Chat.ChatState)
extern void ChatGui_OnChatStateChange_m166507E98B7F98C6E52223B9404BD384411F8BEA (void);
// 0x00000032 System.Void Photon.Chat.Demo.ChatGui::OnSubscribed(System.String[],System.Boolean[])
extern void ChatGui_OnSubscribed_m52519890868FA6E484E27F5E39B02833EB54DDB3 (void);
// 0x00000033 System.Void Photon.Chat.Demo.ChatGui::OnSubscribed(System.String,System.String[],System.Collections.Generic.Dictionary`2<System.Object,System.Object>)
extern void ChatGui_OnSubscribed_mDDD167FAC1109BBC42B8C8C9772AC321D52A5FE4 (void);
// 0x00000034 System.Void Photon.Chat.Demo.ChatGui::InstantiateChannelButton(System.String)
extern void ChatGui_InstantiateChannelButton_mC727F0E0569BAB8AA5DAC9B2EF8A95D49A4A5032 (void);
// 0x00000035 System.Void Photon.Chat.Demo.ChatGui::InstantiateFriendButton(System.String)
extern void ChatGui_InstantiateFriendButton_m24C82847C192124605436330992D637C8A3E7EFB (void);
// 0x00000036 System.Void Photon.Chat.Demo.ChatGui::OnUnsubscribed(System.String[])
extern void ChatGui_OnUnsubscribed_m968F1850ADFE61F878F461E87AF20857A0EE1B94 (void);
// 0x00000037 System.Void Photon.Chat.Demo.ChatGui::OnGetMessages(System.String,System.String[],System.Object[])
extern void ChatGui_OnGetMessages_mE3CAA812F933B7B5C8B4277B56988B18343CEB07 (void);
// 0x00000038 System.Void Photon.Chat.Demo.ChatGui::OnPrivateMessage(System.String,System.Object,System.String)
extern void ChatGui_OnPrivateMessage_mB6EF6D8710F673B8D9B6403F3AF1D13EFEB9269F (void);
// 0x00000039 System.Void Photon.Chat.Demo.ChatGui::OnStatusUpdate(System.String,System.Int32,System.Boolean,System.Object)
extern void ChatGui_OnStatusUpdate_mDE1FAF6055E1758AB9AF5D7B772F9FEA94DB3BDB (void);
// 0x0000003A System.Void Photon.Chat.Demo.ChatGui::OnUserSubscribed(System.String,System.String)
extern void ChatGui_OnUserSubscribed_m2E2516927F409CEF9D5FB44497AD6B113222409A (void);
// 0x0000003B System.Void Photon.Chat.Demo.ChatGui::OnUserUnsubscribed(System.String,System.String)
extern void ChatGui_OnUserUnsubscribed_m2BC2DFEA285C488D13166EBA5698B72550F78654 (void);
// 0x0000003C System.Void Photon.Chat.Demo.ChatGui::OnChannelPropertiesChanged(System.String,System.String,System.Collections.Generic.Dictionary`2<System.Object,System.Object>)
extern void ChatGui_OnChannelPropertiesChanged_mFF2EFCD0C671520155F17F256257E821E51D1926 (void);
// 0x0000003D System.Void Photon.Chat.Demo.ChatGui::OnUserPropertiesChanged(System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.Object,System.Object>)
extern void ChatGui_OnUserPropertiesChanged_m5B5A07B865481CAC5E65D4C28AD060C27EC37DD5 (void);
// 0x0000003E System.Void Photon.Chat.Demo.ChatGui::OnErrorInfo(System.String,System.String,System.Object)
extern void ChatGui_OnErrorInfo_m1EA237F6DAE99E2F52D45C421B2AB1DCD90E4673 (void);
// 0x0000003F System.Void Photon.Chat.Demo.ChatGui::AddMessageToSelectedChannel(System.String)
extern void ChatGui_AddMessageToSelectedChannel_m4E2AF559DB50F8C53DA41268C1175E09AA471A1C (void);
// 0x00000040 System.Void Photon.Chat.Demo.ChatGui::ShowChannel(System.String)
extern void ChatGui_ShowChannel_m696F566CA7F08601752BB3E3508AFA025AC587C3 (void);
// 0x00000041 System.Void Photon.Chat.Demo.ChatGui::OpenDashboard()
extern void ChatGui_OpenDashboard_mEF6A4AC58094283E6942489BED251351E305A01E (void);
// 0x00000042 System.Void Photon.Chat.Demo.ChatGui::.ctor()
extern void ChatGui__ctor_mB1A092C06A5A45E3A7A337A023E08878833B120F (void);
// 0x00000043 System.Void Photon.Chat.Demo.ChatGui::.cctor()
extern void ChatGui__cctor_m7B39238B5EAA662C1906D002E4FB09E9EB0F7100 (void);
// 0x00000044 System.Void Photon.Chat.Demo.FriendItem::set_FriendId(System.String)
extern void FriendItem_set_FriendId_m8359A1EA2AC4624D7D288C1DDC2350975623F7CD (void);
// 0x00000045 System.String Photon.Chat.Demo.FriendItem::get_FriendId()
extern void FriendItem_get_FriendId_mD5028C5F9C1D12F917261C119C45684B3E4B544D (void);
// 0x00000046 System.Void Photon.Chat.Demo.FriendItem::Awake()
extern void FriendItem_Awake_mDEBF0CC81C911D57EBD8690494E68E9F88ED6F4D (void);
// 0x00000047 System.Void Photon.Chat.Demo.FriendItem::OnFriendStatusUpdate(System.Int32,System.Boolean,System.Object)
extern void FriendItem_OnFriendStatusUpdate_m00D8E4A7CFFF6F81A81ACCB5EDFC1142EEAC5718 (void);
// 0x00000048 System.Void Photon.Chat.Demo.FriendItem::.ctor()
extern void FriendItem__ctor_mECF93E0BF74E35EB73BF307FFC9DB30D77343F38 (void);
// 0x00000049 System.Boolean Photon.Chat.Demo.IgnoreUiRaycastWhenInactive::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void IgnoreUiRaycastWhenInactive_IsRaycastLocationValid_m677C47DFEBF604215800A9B741A3300BD0A3947D (void);
// 0x0000004A System.Void Photon.Chat.Demo.IgnoreUiRaycastWhenInactive::.ctor()
extern void IgnoreUiRaycastWhenInactive__ctor_m710699723A0DE8CB36160AACC822FF3198C5F009 (void);
// 0x0000004B System.Void Photon.Chat.Demo.NamePickGui::Start()
extern void NamePickGui_Start_m261A0D587E971FB69F2A604F924BFACE3431E964 (void);
// 0x0000004C System.Void Photon.Chat.Demo.NamePickGui::EndEditOnEnter()
extern void NamePickGui_EndEditOnEnter_m9CBE79633FA01A8D57125C5DEB62629B95C98755 (void);
// 0x0000004D System.Void Photon.Chat.Demo.NamePickGui::StartChat()
extern void NamePickGui_StartChat_m956F5E92C89AB50B4590BC5023FC40023530002E (void);
// 0x0000004E System.Void Photon.Chat.Demo.NamePickGui::.ctor()
extern void NamePickGui__ctor_m2AB2D19C88F5FAFCB399BB58B98BD25B50F19881 (void);
// 0x0000004F System.Void Photon.Chat.UtilityScripts.EventSystemSpawner::OnEnable()
extern void EventSystemSpawner_OnEnable_m92046CF7C2B99309677158A5205E37FA11EE9302 (void);
// 0x00000050 System.Void Photon.Chat.UtilityScripts.EventSystemSpawner::.ctor()
extern void EventSystemSpawner__ctor_m6FCC0DAF89825B22BEDC803B5E02B42FB86CF331 (void);
// 0x00000051 System.Void Photon.Chat.UtilityScripts.OnStartDelete::Start()
extern void OnStartDelete_Start_m033082A59BA5941F5740FAD3CDD2D4501430DD33 (void);
// 0x00000052 System.Void Photon.Chat.UtilityScripts.OnStartDelete::.ctor()
extern void OnStartDelete__ctor_m0B816BC930476E7580303D8E9E8E07444F0C5270 (void);
// 0x00000053 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::Awake()
extern void TextButtonTransition_Awake_m5402B2656DCEF5F78D8BA1D3D7D77EBD7F07AC9E (void);
// 0x00000054 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnEnable()
extern void TextButtonTransition_OnEnable_mD3229078DB78111EDC0B823872D1E381F3C15A5F (void);
// 0x00000055 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnDisable()
extern void TextButtonTransition_OnDisable_mAB2CCC24F8B8F1D664154DC1D0D7E47A925A3B72 (void);
// 0x00000056 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerEnter_m28E7599E058554E4C13D7EA152521BCF4C104B07 (void);
// 0x00000057 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerExit_m25F271B30D439D9AD93483E1CC08FC198A3E9A02 (void);
// 0x00000058 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::.ctor()
extern void TextButtonTransition__ctor_m9E3AFEA1BEFD79F8F0D16DA749243B7D43B78570 (void);
// 0x00000059 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnEnable()
extern void TextToggleIsOnTransition_OnEnable_mA240AFB3178800D5275FB5BD05D54449EC49BAF2 (void);
// 0x0000005A System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnDisable()
extern void TextToggleIsOnTransition_OnDisable_m824A3816D8873B0A5F86F4F401FF080163560404 (void);
// 0x0000005B System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnValueChanged(System.Boolean)
extern void TextToggleIsOnTransition_OnValueChanged_m2C3DAB9655DD596BFC184D0A5B4F83D36C8B2FFB (void);
// 0x0000005C System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerEnter_m01E7951E3FE9C9C9A1FC1D5E26EFF4041799E0E5 (void);
// 0x0000005D System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerExit_mBF6DDD1410475F0C6118F9C85F572E669FBB4C20 (void);
// 0x0000005E System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::.ctor()
extern void TextToggleIsOnTransition__ctor_mA401C98E0CC8F9B8728B46138CCF941C6E471F04 (void);
static Il2CppMethodPointer s_methodPointers[94] = 
{
	Launcher_Awake_m51131F9D53E49BF22DBF64A5B3CC40A475058D1A,
	Launcher_Start_mBD1E6306A64712A170916699B4BE6AA6BD32E37E,
	Launcher_Update_mD70990363AAEAB75790E996E40E5DADEC5D73C11,
	Launcher_OnConnectedToMaster_mBE4CB3AA2DF5AF2BE55F7A31C7520372408F75A5,
	Launcher_OnDisconnected_m00637F435FF952B3D2CDB83A808F50150695D4A4,
	Launcher_Connect_m4AF199D6BD9C325D0750800B2980D666029AE718,
	Launcher__ctor_mB177F9E0B472FC0208A5EF8E702BD3712C0AED96,
	ConnectAndJoinRandomLb_Start_m8784F9A942632995E527C9AAC79D40049C1B4421,
	ConnectAndJoinRandomLb_Update_m27F506BAFEF2C3264B9030218D0ADB86478750E2,
	ConnectAndJoinRandomLb_OnConnected_m5879B131543322FA6CC2D9FB2B4C196698680114,
	ConnectAndJoinRandomLb_OnConnectedToMaster_mCCF5E7C0A7A1A8486F3571F05B2E0662C7C939F2,
	ConnectAndJoinRandomLb_OnDisconnected_m2576DA0537A899046151D7FA51E25AC367F800A7,
	ConnectAndJoinRandomLb_OnCustomAuthenticationResponse_m8F6E456A2C572923AD8E720088C22588A1DB1459,
	ConnectAndJoinRandomLb_OnCustomAuthenticationFailed_m447EA30CA679C12865C767F01D060A3A2EF74C5B,
	ConnectAndJoinRandomLb_OnRegionListReceived_m6F54D1CDC5C51EF9ED8C44E439E83210851CDA8E,
	ConnectAndJoinRandomLb_OnRoomListUpdate_m91AEF2D4CB5CC06810FED71B26B736DDC2938DA0,
	ConnectAndJoinRandomLb_OnLobbyStatisticsUpdate_m02972ED26C4B5FCECE1E7A0B482A32E53363F2F7,
	ConnectAndJoinRandomLb_OnJoinedLobby_m171BA1094BCA0FC7A1D13EA07C858CD2D2F423EF,
	ConnectAndJoinRandomLb_OnLeftLobby_m1BCA515302514D0EF88687D2216D67A9D724B0F4,
	ConnectAndJoinRandomLb_OnFriendListUpdate_m6E1EBEFEC84BA9FFA5296B547431BDB692E053DB,
	ConnectAndJoinRandomLb_OnCreatedRoom_mB1906ADF16C593829B0DD191F319716A97B10583,
	ConnectAndJoinRandomLb_OnCreateRoomFailed_mD11765F8AA8A45B8AC5555207281A43ABA829D24,
	ConnectAndJoinRandomLb_OnJoinedRoom_mCE9BDE69A13EA84E9ACA73E064A695B9D3754E8B,
	ConnectAndJoinRandomLb_OnJoinRoomFailed_m5622896774D36D4960735E32F353869D9D6DF94C,
	ConnectAndJoinRandomLb_OnJoinRandomFailed_m4BCA35CBE444C5AD28D7D713E92E893198F91B69,
	ConnectAndJoinRandomLb_OnLeftRoom_m488901B35215F9B70BACC7A08ABB123F3B605137,
	ConnectAndJoinRandomLb_OnRegionPingCompleted_mD76EE8F39A7AE8EF7E071B9E1C5D1A52452FEF17,
	ConnectAndJoinRandomLb__ctor_mD06217DD59635BD5DC39E8E14274D6604FDF85C1,
	AppSettingsExtensions_GetChatSettings_m32D1D0AF37E161CF26E9180F659F6657F6F24D00,
	ChannelSelector_SetChannel_m29F99876C0AF817AEFBDB5539DF70F23558D2A7E,
	ChannelSelector_OnPointerClick_mA5A9AB30717F59D434855F47CAF95543C7A64B9C,
	ChannelSelector__ctor_mB3DF2FA2ED74E01976D4D3ACFB50CB096FEF18F0,
	ChatAppIdCheckerUI_Update_m1D3579AE4EE35E468A353AA67EBD5D9F7C87E2E9,
	ChatAppIdCheckerUI__ctor_mA1D182C1AED55533944CD356D13AA447574B7870,
	ChatGui_get_UserName_mE13C9B7C725EC8F90975DE2D4ED7F3CC28596ACF,
	ChatGui_set_UserName_m8955E5E94B1B8469E4FCD87D10041F53A80E9E0B,
	ChatGui_Start_m9C36C012AF6740959AD6855FCE3C7156D42B636A,
	ChatGui_Connect_mF3437B3F3E848B574D498715B4FB45CBB8A769CD,
	ChatGui_OnDestroy_mA771028772B2BE041F6A8611CB67E6E369654D8E,
	ChatGui_OnApplicationQuit_mCF2A69BA18FF51991BB0F7F5A6898B9C8D19B099,
	ChatGui_Update_mF777D28857ABC30C851D6DD32B3A36BBE7880958,
	ChatGui_OnEnterSend_mD1E43BC5C5ED3F65C8287C7CCB2D09BE0E508375,
	ChatGui_OnClickSend_mA34A7BDF93AF080B0926A4D503AC77035C6345D7,
	ChatGui_SendChatMessage_mC76BD609B9B8E024027BD69E1915E4577B65E76F,
	ChatGui_PostHelpToCurrentChannel_m14AD0594316C6C38A2B85F390B52595A1CF8FD17,
	ChatGui_DebugReturn_m7E7552B5CD94F91CE5C41CD7813EDE70B6176567,
	ChatGui_OnConnected_m63335F6ADB99CA4C1DBD70CE3D3BE669DE0E53B9,
	ChatGui_OnDisconnected_m4AAE28BB7B47BAD132651AFC8E27F573A36E3AFF,
	ChatGui_OnChatStateChange_m166507E98B7F98C6E52223B9404BD384411F8BEA,
	ChatGui_OnSubscribed_m52519890868FA6E484E27F5E39B02833EB54DDB3,
	ChatGui_OnSubscribed_mDDD167FAC1109BBC42B8C8C9772AC321D52A5FE4,
	ChatGui_InstantiateChannelButton_mC727F0E0569BAB8AA5DAC9B2EF8A95D49A4A5032,
	ChatGui_InstantiateFriendButton_m24C82847C192124605436330992D637C8A3E7EFB,
	ChatGui_OnUnsubscribed_m968F1850ADFE61F878F461E87AF20857A0EE1B94,
	ChatGui_OnGetMessages_mE3CAA812F933B7B5C8B4277B56988B18343CEB07,
	ChatGui_OnPrivateMessage_mB6EF6D8710F673B8D9B6403F3AF1D13EFEB9269F,
	ChatGui_OnStatusUpdate_mDE1FAF6055E1758AB9AF5D7B772F9FEA94DB3BDB,
	ChatGui_OnUserSubscribed_m2E2516927F409CEF9D5FB44497AD6B113222409A,
	ChatGui_OnUserUnsubscribed_m2BC2DFEA285C488D13166EBA5698B72550F78654,
	ChatGui_OnChannelPropertiesChanged_mFF2EFCD0C671520155F17F256257E821E51D1926,
	ChatGui_OnUserPropertiesChanged_m5B5A07B865481CAC5E65D4C28AD060C27EC37DD5,
	ChatGui_OnErrorInfo_m1EA237F6DAE99E2F52D45C421B2AB1DCD90E4673,
	ChatGui_AddMessageToSelectedChannel_m4E2AF559DB50F8C53DA41268C1175E09AA471A1C,
	ChatGui_ShowChannel_m696F566CA7F08601752BB3E3508AFA025AC587C3,
	ChatGui_OpenDashboard_mEF6A4AC58094283E6942489BED251351E305A01E,
	ChatGui__ctor_mB1A092C06A5A45E3A7A337A023E08878833B120F,
	ChatGui__cctor_m7B39238B5EAA662C1906D002E4FB09E9EB0F7100,
	FriendItem_set_FriendId_m8359A1EA2AC4624D7D288C1DDC2350975623F7CD,
	FriendItem_get_FriendId_mD5028C5F9C1D12F917261C119C45684B3E4B544D,
	FriendItem_Awake_mDEBF0CC81C911D57EBD8690494E68E9F88ED6F4D,
	FriendItem_OnFriendStatusUpdate_m00D8E4A7CFFF6F81A81ACCB5EDFC1142EEAC5718,
	FriendItem__ctor_mECF93E0BF74E35EB73BF307FFC9DB30D77343F38,
	IgnoreUiRaycastWhenInactive_IsRaycastLocationValid_m677C47DFEBF604215800A9B741A3300BD0A3947D,
	IgnoreUiRaycastWhenInactive__ctor_m710699723A0DE8CB36160AACC822FF3198C5F009,
	NamePickGui_Start_m261A0D587E971FB69F2A604F924BFACE3431E964,
	NamePickGui_EndEditOnEnter_m9CBE79633FA01A8D57125C5DEB62629B95C98755,
	NamePickGui_StartChat_m956F5E92C89AB50B4590BC5023FC40023530002E,
	NamePickGui__ctor_m2AB2D19C88F5FAFCB399BB58B98BD25B50F19881,
	EventSystemSpawner_OnEnable_m92046CF7C2B99309677158A5205E37FA11EE9302,
	EventSystemSpawner__ctor_m6FCC0DAF89825B22BEDC803B5E02B42FB86CF331,
	OnStartDelete_Start_m033082A59BA5941F5740FAD3CDD2D4501430DD33,
	OnStartDelete__ctor_m0B816BC930476E7580303D8E9E8E07444F0C5270,
	TextButtonTransition_Awake_m5402B2656DCEF5F78D8BA1D3D7D77EBD7F07AC9E,
	TextButtonTransition_OnEnable_mD3229078DB78111EDC0B823872D1E381F3C15A5F,
	TextButtonTransition_OnDisable_mAB2CCC24F8B8F1D664154DC1D0D7E47A925A3B72,
	TextButtonTransition_OnPointerEnter_m28E7599E058554E4C13D7EA152521BCF4C104B07,
	TextButtonTransition_OnPointerExit_m25F271B30D439D9AD93483E1CC08FC198A3E9A02,
	TextButtonTransition__ctor_m9E3AFEA1BEFD79F8F0D16DA749243B7D43B78570,
	TextToggleIsOnTransition_OnEnable_mA240AFB3178800D5275FB5BD05D54449EC49BAF2,
	TextToggleIsOnTransition_OnDisable_m824A3816D8873B0A5F86F4F401FF080163560404,
	TextToggleIsOnTransition_OnValueChanged_m2C3DAB9655DD596BFC184D0A5B4F83D36C8B2FFB,
	TextToggleIsOnTransition_OnPointerEnter_m01E7951E3FE9C9C9A1FC1D5E26EFF4041799E0E5,
	TextToggleIsOnTransition_OnPointerExit_mBF6DDD1410475F0C6118F9C85F572E669FBB4C20,
	TextToggleIsOnTransition__ctor_mA401C98E0CC8F9B8728B46138CCF941C6E471F04,
};
static const int32_t s_InvokerIndices[94] = 
{
	2810,
	2810,
	2810,
	2810,
	2315,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2315,
	2329,
	2329,
	2329,
	2329,
	2329,
	2810,
	2810,
	2329,
	2810,
	1177,
	2810,
	1177,
	1177,
	2810,
	2329,
	2810,
	4389,
	2329,
	2329,
	2810,
	2810,
	2810,
	2774,
	2329,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2329,
	2810,
	1381,
	2810,
	2810,
	2315,
	1353,
	847,
	2329,
	2329,
	2329,
	847,
	847,
	543,
	1353,
	1353,
	847,
	557,
	847,
	2329,
	2329,
	2810,
	2810,
	4542,
	2329,
	2774,
	2810,
	812,
	2810,
	1092,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2810,
	2329,
	2329,
	2810,
	2810,
	2810,
	2348,
	2329,
	2329,
	2810,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	94,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
